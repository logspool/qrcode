# qrcode

#### 介绍
  开源免费的二维码批量生成工具，可以按自己需要的规则批量生成二维码，比如数字自增二维码、随机生成二维码、按日期批量生成二维码。同时软件不仅免费还开源，永久不收费。http://www.itxst.com/qrcode

#### 软件架构
开发工具：visual studio 2017
开发语言 C# 4.0  sqlite  

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
